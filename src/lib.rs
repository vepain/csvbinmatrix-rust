#![doc = include_str!("../README.md")]
#![warn(missing_docs)]
#![warn(clippy::pedantic)]
pub mod filters;
pub mod matrix;

#[doc = include_str!("../docs/prelude.md")]
pub mod prelude {
    pub use crate::filters::{BoolVecDimensionFilter, ClosureDimensionFilter, DimensionFilter};
    pub use crate::matrix::items::Coordinates;
    pub use crate::matrix::CSVBinaryMatrix;
}
