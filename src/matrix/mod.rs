#![doc = include_str!("../../docs/matrix/mod.md")]

pub mod create;
pub mod io;
pub mod items;
pub mod iter;
pub mod update;
pub mod views;

/// Binary matrix Compressed Sparse Vector.
///
/// For all the example sections, we use `matrix` is the following matrix:
/// ```text
/// 0 0 0
/// 0 0 1
/// 0 1 1
/// 1 1 1
/// ```
///
/// You can obtain this matrix with the following code:
///
/// ```rust
/// use csvbinmatrix::prelude::CSVBinaryMatrix;
///
/// let matrix = CSVBinaryMatrix::try_from(&[
///     [0, 0, 0],
///     [0, 0, 1],
///     [0, 1, 1],
///     [1, 1, 1],
/// ]).unwrap();
/// ```
///
/// See [`csvbinmatrix::matrix::create`](crate::matrix::create) module for more details.
///
#[derive(Debug, PartialEq, Clone)]
#[allow(clippy::module_name_repetitions)]
pub struct CSVBinaryMatrix {
    number_of_rows: usize,
    number_of_columns: usize,
    distances: Vec<usize>,
}

impl CSVBinaryMatrix {
    /// Returns the number of rows.
    #[must_use]
    pub fn number_of_rows(&self) -> usize {
        self.number_of_rows
    }

    /// Returns the number of columns.
    #[must_use]
    pub fn number_of_columns(&self) -> usize {
        self.number_of_columns
    }

    /// Returns the number of cells.
    #[must_use]
    pub fn number_of_cells(&self) -> usize {
        self.number_of_rows * self.number_of_columns
    }

    /// Returns the number of ones.
    #[must_use]
    pub fn number_of_ones(&self) -> usize {
        self.distances.len() - 1
    }

    /// Returns the number of zeros.
    #[must_use]
    pub fn number_of_zeros(&self) -> usize {
        self.number_of_rows * self.number_of_columns - self.number_of_ones()
    }

    /// Returns the sparsity of the matrix.
    ///
    /// The sparsity is the ratio of the number of zeros to the number of cells.
    #[must_use]
    pub fn sparsity(&self) -> f64 {
        let number_of_cells = self.number_of_rows * self.number_of_columns;
        #[allow(clippy::cast_precision_loss)]
        {
            1.0 - ((self.distances.len() - 1) as f64 / number_of_cells as f64)
        }
    }

    /// Returns the density of the matrix.
    ///
    /// The density is the ratio of the number of ones to the number of cells.
    #[must_use]
    pub fn density(&self) -> f64 {
        let number_of_cells = self.number_of_rows * self.number_of_columns;
        #[allow(clippy::cast_precision_loss)]
        {
            (self.distances.len() - 1) as f64 / number_of_cells as f64
        }
    }

    /// Returns `true` if the matrix is complete or all-ones.
    #[must_use]
    pub fn is_complete(&self) -> bool {
        self.number_of_ones() == self.number_of_rows * self.number_of_columns
    }

    /// Returns `true` if the matrix is empty or all-zeros.
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.number_of_ones() == 0
    }
}

/* ============================================================================================== */
/*                                              TESTS                                             */
/* ============================================================================================== */
#[cfg(test)]
mod tests {

    use rstest::{fixture, rstest};

    use super::CSVBinaryMatrix;

    /// A CSVBinaryMatrix with all zeros
    ///
    /// ```text
    /// 0 0 0
    /// 0 0 0
    /// 0 0 0
    /// ```
    #[fixture]
    pub fn zeros_matrix() -> CSVBinaryMatrix {
        CSVBinaryMatrix {
            number_of_rows: 3,
            number_of_columns: 3,
            distances: vec![8],
        }
    }

    /// A CSVBinaryMatrix with all ones
    ///
    /// ```text
    /// 1 1 1
    /// 1 1 1
    /// 1 1 1
    /// ```
    #[fixture]
    pub fn ones_matrix() -> CSVBinaryMatrix {
        CSVBinaryMatrix {
            number_of_rows: 3,
            number_of_columns: 3,
            distances: vec![0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        }
    }

    /// A CSVBinaryMatrix
    ///
    /// ```text
    /// 1 1 1
    /// 1 0 0
    /// 0 1 0
    /// ```
    #[fixture]
    pub fn matrix_a() -> CSVBinaryMatrix {
        CSVBinaryMatrix {
            number_of_rows: 3,
            number_of_columns: 3,
            distances: vec![0, 1, 1, 1, 4, 1],
        }
    }

    #[rstest]
    fn debug(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
        assert_eq!(
            format!("{zeros_matrix:?}"),
            "CSVBinaryMatrix { number_of_rows: 3, number_of_columns: 3, distances: [8] }"
        );
        assert_eq!(
                format!("{matrix_a:?}"),
                "CSVBinaryMatrix { number_of_rows: 3, number_of_columns: 3, distances: [0, 1, 1, 1, 4, 1] }"
            );
    }

    #[rstest]
    fn number_of_rows(matrix_a: CSVBinaryMatrix) {
        assert_eq!(matrix_a.number_of_rows(), 3);
    }

    #[rstest]
    fn number_of_columns(matrix_a: CSVBinaryMatrix) {
        assert_eq!(matrix_a.number_of_columns(), 3);
    }

    #[rstest]
    fn number_of_cells(matrix_a: CSVBinaryMatrix) {
        assert_eq!(matrix_a.number_of_cells(), 9);
    }

    #[rstest]
    fn number_of_ones(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
        assert_eq!(zeros_matrix.number_of_ones(), 0);
        assert_eq!(matrix_a.number_of_ones(), 5);
    }

    #[rstest]
    fn number_of_zeros(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
        assert_eq!(zeros_matrix.number_of_zeros(), 9);
        assert_eq!(matrix_a.number_of_zeros(), 4);
    }

    #[rstest]
    fn sparsity(
        zeros_matrix: CSVBinaryMatrix,
        ones_matrix: CSVBinaryMatrix,
        matrix_a: CSVBinaryMatrix,
    ) {
        let error_margin = f64::EPSILON;
        assert!((1.0 - zeros_matrix.sparsity()).abs() < error_margin);
        assert!(ones_matrix.sparsity() < error_margin);
        assert!((0.444_444_444_444_444_4 - matrix_a.sparsity()).abs() < error_margin);
    }

    #[rstest]
    fn density(
        zeros_matrix: CSVBinaryMatrix,
        ones_matrix: CSVBinaryMatrix,
        matrix_a: CSVBinaryMatrix,
    ) {
        let error_margin = f64::EPSILON;
        assert!(zeros_matrix.density() < error_margin);
        assert!((1.0 - ones_matrix.density()).abs() < error_margin);
        assert!((0.555_555_555_555_555_6 - matrix_a.density()).abs() < error_margin);
    }

    #[rstest]
    fn is_complete(zeros_matrix: CSVBinaryMatrix, ones_matrix: CSVBinaryMatrix) {
        assert!(!zeros_matrix.is_complete());
        assert!(ones_matrix.is_complete());
    }

    #[rstest]
    fn is_empty(zeros_matrix: CSVBinaryMatrix, ones_matrix: CSVBinaryMatrix) {
        assert!(zeros_matrix.is_empty());
        assert!(!ones_matrix.is_empty());
    }
}
