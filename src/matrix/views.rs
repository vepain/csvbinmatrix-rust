#![doc = include_str!("../../docs/matrix/views.md")]

use thiserror::Error;

use super::items::Coordinates;
use super::iter::OnesCoordinatesColumnCursor;
use super::CSVBinaryMatrix;
use crate::filters::DimensionFilter;

/// The resulting matrix is degenerated.
#[derive(Error, Debug, PartialEq)]
#[error("The resulting matrix is degenerated ({number_of_rows}x{number_of_columns})")]
pub struct DegeneratedMatrixError {
    number_of_rows: usize,
    number_of_columns: usize,
}

struct SubCSVBMFactory<'filter, R: DimensionFilter, C: DimensionFilter> {
    number_of_rows: usize,
    number_of_columns: usize,
    coordinates_prev: Coordinates,
    row_number_correction: usize,
    col_number_correction: usize,
    distances: Vec<usize>,
    row_filter: &'filter R,
    column_filter: &'filter C,
}

impl<'filter, R, C> SubCSVBMFactory<'filter, R, C>
where
    R: DimensionFilter,
    C: DimensionFilter,
{
    fn new(
        super_number_of_rows: usize,
        super_number_of_cols: usize,
        row_filter: &'filter R,
        column_filter: &'filter C,
    ) -> Self {
        Self {
            number_of_rows: (0..super_number_of_rows)
                .filter(|&i| row_filter.accepts(i))
                .count(),
            number_of_columns: (0..super_number_of_cols)
                .filter(|&j| column_filter.accepts(j))
                .count(),
            coordinates_prev: Coordinates::new(0, 0),
            row_number_correction: 0,
            col_number_correction: 0,
            distances: Vec::new(),
            row_filter,
            column_filter,
        }
    }

    fn new_reverse(
        super_number_of_rows: usize,
        super_number_of_cols: usize,
        row_filter: &'filter R,
        column_filter: &'filter C,
    ) -> Self {
        let sub_number_of_rows = (0..super_number_of_rows)
            .filter(|&i| row_filter.accepts(i))
            .count();
        let sub_number_of_cols = (0..super_number_of_cols)
            .filter(|&j| column_filter.accepts(j))
            .count();
        let curr_row = match sub_number_of_rows {
            0 => 0,
            _ => sub_number_of_rows - 1,
        };
        let curr_col = match sub_number_of_cols {
            0 => 0,
            _ => sub_number_of_cols - 1,
        };
        Self {
            number_of_rows: sub_number_of_rows,
            number_of_columns: sub_number_of_cols,
            coordinates_prev: Coordinates::new(curr_row, curr_col),
            row_number_correction: super_number_of_rows - sub_number_of_rows,
            col_number_correction: super_number_of_cols - sub_number_of_cols,
            distances: Vec::new(),
            row_filter,
            column_filter,
        }
    }

    fn is_degenerated(&self) -> bool {
        self.number_of_rows == 0 || self.number_of_columns == 0
    }

    fn update_row_correction(
        &mut self,
        coordinates_prev: &Coordinates,
        coordinates_curr: &Coordinates,
    ) {
        if coordinates_prev.row() < coordinates_curr.row() {
            self.row_number_correction += (coordinates_prev.row()..coordinates_curr.row())
                .filter(|&i| !self.row_filter.accepts(i))
                .count();
        }
    }

    fn update_col_correction(
        &mut self,
        coordinates_prev: &Coordinates,
        coordinates_curr: &Coordinates,
    ) {
        match coordinates_prev.column().cmp(&coordinates_curr.column()) {
            std::cmp::Ordering::Greater => {
                self.col_number_correction = (0..coordinates_curr.column())
                    .filter(|&j| !self.column_filter.accepts(j))
                    .count();
            }
            std::cmp::Ordering::Less => {
                self.col_number_correction += (coordinates_prev.column()
                    ..coordinates_curr.column())
                    .filter(|&j| !self.column_filter.accepts(j))
                    .count();
            }
            std::cmp::Ordering::Equal => {}
        }
    }

    fn update_row_correction_backward(
        &mut self,
        coordinates_prev: &Coordinates,
        coordinates_curr: &Coordinates,
    ) {
        if coordinates_curr.row() < coordinates_prev.row() {
            self.row_number_correction -= (coordinates_curr.row()..coordinates_prev.row())
                .filter(|&i| !self.row_filter.accepts(i))
                .count();
        }
    }

    fn update_col_correction_backward(
        &mut self,
        coordinates_prev: &Coordinates,
        coordinates_curr: &Coordinates,
        super_number_of_cols: usize,
    ) {
        match coordinates_prev.column().cmp(&coordinates_curr.column()) {
            std::cmp::Ordering::Greater => {
                self.col_number_correction -= (coordinates_curr.column()
                    ..coordinates_prev.column())
                    .filter(|&j| !self.column_filter.accepts(j))
                    .count();
            }
            std::cmp::Ordering::Less => {
                self.col_number_correction = super_number_of_cols
                    - self.number_of_columns
                    - (coordinates_curr.column()..super_number_of_cols)
                        .filter(|&j| !self.column_filter.accepts(j))
                        .count();
            }
            std::cmp::Ordering::Equal => {}
        }
    }

    fn process_the_one(&mut self, super_coordinates_curr: &Coordinates) {
        if self.row_filter.accepts(super_coordinates_curr.row())
            && self.column_filter.accepts(super_coordinates_curr.column())
        {
            let coordinates_curr = Coordinates::new(
                super_coordinates_curr.row() - self.row_number_correction,
                super_coordinates_curr.column() - self.col_number_correction,
            );

            let mut sub_distance =
                (coordinates_curr.row() - self.coordinates_prev.row()) * self.number_of_columns;
            if coordinates_curr.column() < self.coordinates_prev.column() {
                sub_distance -= self.coordinates_prev.column() - coordinates_curr.column();
            } else {
                sub_distance += coordinates_curr.column() - self.coordinates_prev.column();
            }
            self.distances.push(sub_distance);
            self.coordinates_prev = coordinates_curr;
        }
    }

    fn process_the_one_backward(&mut self, super_coordinates_curr: &Coordinates) {
        if self.row_filter.accepts(super_coordinates_curr.row())
            && self.column_filter.accepts(super_coordinates_curr.column())
        {
            let coordinates_curr = Coordinates::new(
                super_coordinates_curr.row() - self.row_number_correction,
                super_coordinates_curr.column() - self.col_number_correction,
            );
            let mut sub_distance =
                (self.coordinates_prev.row() - coordinates_curr.row()) * self.number_of_columns;
            if coordinates_curr.column() >= self.coordinates_prev.column() {
                sub_distance -= coordinates_curr.column() - self.coordinates_prev.column();
            } else {
                sub_distance += self.coordinates_prev.column() - coordinates_curr.column();
            }
            self.distances.push(sub_distance);
            self.coordinates_prev = coordinates_curr;
        }
    }

    fn build_csvbm(mut self) -> Result<CSVBinaryMatrix, DegeneratedMatrixError> {
        if self.is_degenerated() {
            Err(DegeneratedMatrixError {
                number_of_rows: self.number_of_rows,
                number_of_columns: self.number_of_columns,
            })
        } else {
            // distance = (number_of_rows - 1 - prev_row) * number_of_columns
            // + (number_of_columns - 1 - prev_col);
            self.distances.push(
                (self.number_of_rows - 1 - self.coordinates_prev.row()) * self.number_of_columns
                    + (self.number_of_columns - 1 - self.coordinates_prev.column()),
            );
            Ok(CSVBinaryMatrix {
                number_of_rows: self.number_of_rows,
                number_of_columns: self.number_of_columns,
                distances: self.distances,
            })
        }
    }

    fn build_csvbm_backward(mut self) -> Result<CSVBinaryMatrix, DegeneratedMatrixError> {
        if self.is_degenerated() {
            Err(DegeneratedMatrixError {
                number_of_rows: self.number_of_rows,
                number_of_columns: self.number_of_columns,
            })
        } else {
            self.distances.push(
                self.coordinates_prev.row() * self.number_of_columns
                    + self.coordinates_prev.column(),
            );
            Ok(CSVBinaryMatrix {
                number_of_rows: self.number_of_rows,
                number_of_columns: self.number_of_columns,
                distances: self.distances,
            })
        }
    }
}

impl CSVBinaryMatrix {
    /// Reverse the rows and the columns of the [`CSVBinaryMatrix`] in place.
    ///
    /// Memory and time complexities are linear according to the number ones.
    ///
    /// If you just need to read the coordinate in reversed order, use [`CSVBinaryMatrix::iter_ones_coordinates_from_end`].
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// # let mut matrix = CSVBinaryMatrix::try_from(&[
    /// #     [0, 0, 0],
    /// #     [0, 0, 1],
    /// #     [0, 1, 1],
    /// #     [1, 1, 1],
    /// # ]).unwrap();
    /// #
    /// matrix.reverse();
    ///
    /// assert_eq!(matrix, CSVBinaryMatrix::try_from(&[
    ///     [1, 1, 1],
    ///     [1, 1, 0],
    ///     [1, 0, 0],
    ///     [0, 0, 0],
    /// ]).unwrap());
    pub fn reverse(&mut self) {
        self.distances.reverse();
    }

    /// Returns the matrix where the rows and the columns of self are reversed.
    ///
    /// Memory and time complexities are linear according to the number ones.
    /// The given matrix is consumed while the resulting reversed matrix is filled.
    ///
    /// If you just need to read the coordinate in reversed order, use [`CSVBinaryMatrix::iter_ones_coordinates_from_end`].
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// assert_eq!(
    ///     CSVBinaryMatrix::try_from(&[
    ///         [0, 0, 0],
    ///         [0, 0, 1],
    ///         [0, 1, 1],
    ///         [1, 1, 1],
    ///     ]).unwrap().into_reversed(),
    ///     CSVBinaryMatrix::try_from(&[
    ///         [1, 1, 1],
    ///         [1, 1, 0],
    ///         [1, 0, 0],
    ///         [0, 0, 0],
    ///     ]).unwrap()
    /// );
    /// ```
    #[must_use]
    pub fn into_reversed(mut self) -> Self {
        // OPTIMIZE is distances.drain(..).rev().collect() more efficient?
        let mut distances_reversed = Vec::with_capacity(self.distances.len());
        while let Some(distance) = self.distances.pop() {
            distances_reversed.push(distance);
        }
        Self {
            number_of_rows: self.number_of_rows,
            number_of_columns: self.number_of_columns,
            distances: distances_reversed,
        }
    }

    /// Return a sub [`CSVBinaryMatrix`] from the given [`CSVBinaryMatrix`].
    ///
    /// # Example
    ///
    /// ```rust
    /// use csvbinmatrix::prelude::{CSVBinaryMatrix, ClosureDimensionFilter};
    ///
    /// let matrix = CSVBinaryMatrix::try_from(&[
    ///     [0, 0, 0],
    ///     [0, 0, 1],
    ///     [0, 1, 1],
    ///     [1, 1, 1],
    /// ]).unwrap();
    /// let expected_sub_matrix = CSVBinaryMatrix::try_from(&vec![vec![0, 0], vec![1, 1], vec![1, 1]]).unwrap();
    ///
    /// let row_filter = ClosureDimensionFilter::new(|i| i != 1);
    /// let column_filter = ClosureDimensionFilter::new(|j| j != 0);
    ///
    /// // Generate submatrices according to the couples of row and column filters.
    /// // `matrix` is not consumed.
    /// match matrix.to_submatrix(&row_filter, &column_filter) {
    ///     Ok(sub_matrix) => assert_eq!(sub_matrix, expected_sub_matrix),
    ///     Err(err) => panic!("[ERROR] {err}"),
    /// }
    /// ```
    ///
    /// # Errors
    ///
    /// * [`DegeneratedMatrixError`] if the submatrix is degenerated.
    ///
    /// # Panics
    ///
    /// Unreachable: if there are no distances in the distance list.
    pub fn to_submatrix<R, C>(
        &self,
        row_filter: &R,
        column_filter: &C,
    ) -> Result<CSVBinaryMatrix, DegeneratedMatrixError>
    where
        R: DimensionFilter,
        C: DimensionFilter,
    {
        let mut factory = SubCSVBMFactory::new(
            self.number_of_rows,
            self.number_of_columns,
            row_filter,
            column_filter,
        );

        if factory.is_degenerated() {
            return factory.build_csvbm();
        }

        let mut coordinates_prev = Coordinates::new(self.number_of_rows, self.number_of_columns);
        let mut coordinates_curr: Coordinates;
        let mut ones_coordinates_column_cursor_curr =
            OnesCoordinatesColumnCursor::new(self.number_of_columns, Coordinates::new(0, 0));

        let mut distance_index = 0;
        let mut distance: usize;

        while distance_index < self.distances.len() - 1 {
            unsafe {
                distance = *self.distances.get_unchecked(distance_index);
            }

            coordinates_curr = ones_coordinates_column_cursor_curr.forward(distance);

            factory.update_row_correction(&coordinates_prev, &coordinates_curr);
            factory.update_col_correction(&coordinates_prev, &coordinates_curr);
            factory.process_the_one(&coordinates_curr);

            coordinates_prev = coordinates_curr;
            distance_index += 1;
        }
        factory.build_csvbm()
    }

    /// Return several sub [`CSVBinaryMatrix`] from the given [`CSVBinaryMatrix`] with row and column reversed.
    ///
    /// This function is really efficient as it creates a new [`CSVBinaryMatrix`] while consuming the given [`CSVBinaryMatrix`].
    /// The memory and the time complexities are linear according to the number ones of the super binary matrix.
    ///
    /// # Warning
    ///
    /// Even if the row/columns filters accept all the rows/columns, the rows and the columns of the resulting submatrix are reversed.
    ///
    /// For example, let the initial matrix to be
    /// ```md
    /// 0 0 0
    /// 0 0 1
    /// 0 1 1
    /// 1 1 1
    /// ```
    /// Then the submatrix (with row/column filters set to true for all the elements) will be
    /// ```md
    /// 1 1 1
    /// 1 1 0
    /// 1 0 0
    /// 0 0 0
    /// ```
    /// # Example
    ///
    /// ```rust
    /// use csvbinmatrix::prelude::{CSVBinaryMatrix, BoolVecDimensionFilter, ClosureDimensionFilter};
    ///
    /// let matrix = CSVBinaryMatrix::try_from(&[
    ///     [0, 0, 0],
    ///     [0, 0, 1],
    ///     [0, 1, 1],
    ///     [1, 1, 1],
    /// ]).unwrap();
    ///
    /// let expected_sub_matrix = CSVBinaryMatrix::try_from(&[[0, 0], [1, 1], [1, 1]]).unwrap();
    ///
    /// // Filter with a boolean vector:
    /// // * to represent complex truth states
    /// // * costly
    /// let row_filter =
    ///     match BoolVecDimensionFilter::new(vec![true, false, true, true], matrix.number_of_rows()) {
    ///         Ok(filter) => filter,
    ///         Err(err) => panic!("[ERROR] {err}"),
    ///     };
    ///
    /// // Filter with a closure
    /// // * to represent simple truth states
    /// // * efficient
    /// let column_filter = ClosureDimensionFilter::new(|j| j != 0);
    ///
    /// // Generate submatrices according to the couples of row and column filters.
    /// // `matrix` is consumed.
    /// let mut sub_matrices = matrix.into_reversed_submatrices(vec![(&row_filter, &column_filter)]);
    ///
    /// // The rows and the columns of the submatrices are reversed comparing to the ones of the super matrix.
    /// match sub_matrices.pop() {
    ///     Some(Ok(reversed_sub_matrix)) => {
    ///         assert_eq!(reversed_sub_matrix.into_reversed(), expected_sub_matrix)
    ///     }
    ///     Some(Err(err)) => panic!("[ERROR] {err}"),
    ///     _ => unreachable!("There must be one resulting sub matrix."),
    /// }
    /// // We give only one filter couple, so there is only one resulting submatrix.
    /// assert!(sub_matrices.pop().is_none());
    /// ```
    ///
    /// # Errors
    ///
    /// * [`DegeneratedMatrixError`] if the submatrix is degenerated.
    ///
    /// # Panics
    ///
    /// Unreachable: if there are no distances in the distance list.
    #[must_use]
    pub fn into_reversed_submatrices<R, C>(
        mut self,
        mut row_col_filters: Vec<(&R, &C)>,
    ) -> Vec<Result<CSVBinaryMatrix, DegeneratedMatrixError>>
    where
        R: DimensionFilter,
        C: DimensionFilter,
    {
        let mut factories: Vec<SubCSVBMFactory<R, C>> = Vec::with_capacity(row_col_filters.len());
        while let Some((row_filter, column_filter)) = row_col_filters.pop() {
            factories.push(SubCSVBMFactory::new_reverse(
                self.number_of_rows,
                self.number_of_columns,
                row_filter,
                column_filter,
            ));
        }
        let mut ok_factories = vec![];
        for factory in &mut factories {
            if !factory.is_degenerated() {
                ok_factories.push(factory);
            }
        }

        let mut coordinates_prev = Coordinates::new(self.number_of_rows, self.number_of_columns);
        let mut coordinates_curr: Coordinates;
        let mut ones_coordinates_column_cursor_curr = OnesCoordinatesColumnCursor::new(
            self.number_of_columns,
            Coordinates::new(self.number_of_rows - 1, self.number_of_columns - 1),
        );

        let mut distance_index = self.distances.len() - 1;
        let mut distance: usize;

        // OPTIMIZE is for distance in self.distances.drain(1..).rev() better?
        while distance_index > 0 {
            distance = self.distances.pop().unwrap();

            coordinates_curr = ones_coordinates_column_cursor_curr.unchecked_backward(distance);

            for factory in &mut ok_factories {
                factory.update_row_correction_backward(&coordinates_prev, &coordinates_curr);
                factory.update_col_correction_backward(
                    &coordinates_prev,
                    &coordinates_curr,
                    self.number_of_columns,
                );
                factory.process_the_one_backward(&coordinates_curr);
            }

            coordinates_prev = coordinates_curr;
            distance_index -= 1;
        }
        let mut sub_binmatrices = Vec::with_capacity(factories.len());
        while !factories.is_empty() {
            sub_binmatrices.push(factories.pop().unwrap().build_csvbm_backward());
        }
        sub_binmatrices
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_str_eq;
    use rstest::{fixture, rstest};

    use super::super::tests::{matrix_a, zeros_matrix};
    use super::super::CSVBinaryMatrix;
    use super::DegeneratedMatrixError;
    use crate::filters::ClosureDimensionFilter;

    /// A CSVBinaryMatrix
    ///
    /// ```text
    /// 1 1 0
    /// 0 0 0
    /// 0 1 0
    /// ```
    #[fixture]
    pub fn bin_csv_nullrowcol() -> CSVBinaryMatrix {
        CSVBinaryMatrix {
            number_of_rows: 3,
            number_of_columns: 3,
            distances: vec![0, 1, 6, 1],
        }
    }

    #[rstest]
    fn reverse(zeros_matrix: CSVBinaryMatrix, mut matrix_a: CSVBinaryMatrix) {
        let mut zeros_bin_csv_rev = zeros_matrix.clone();
        zeros_bin_csv_rev.reverse();
        assert_eq!(zeros_bin_csv_rev, zeros_matrix);
        matrix_a.reverse();
        assert_eq!(
            matrix_a,
            CSVBinaryMatrix {
                number_of_rows: 3,
                number_of_columns: 3,
                distances: vec![1, 4, 1, 1, 1, 0],
            }
        );
    }

    #[rstest]
    fn into_reversed(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
        assert_eq!(zeros_matrix.clone().into_reversed(), zeros_matrix);
        assert_eq!(
            matrix_a.clone().into_reversed(),
            CSVBinaryMatrix {
                number_of_rows: 3,
                number_of_columns: 3,
                distances: vec![1, 4, 1, 1, 1, 0],
            }
        );
    }

    #[rstest]
    fn submatrix(
        zeros_matrix: CSVBinaryMatrix,
        matrix_a: CSVBinaryMatrix,
        bin_csv_nullrowcol: CSVBinaryMatrix,
    ) {
        assert_eq!(
            zeros_matrix
                .to_submatrix(
                    &ClosureDimensionFilter::new(|_| true),
                    &ClosureDimensionFilter::new(|_| true)
                )
                .unwrap(),
            zeros_matrix
        );

        match zeros_matrix.to_submatrix(
            &ClosureDimensionFilter::new(|_| false),
            &ClosureDimensionFilter::new(|_| false),
        ) {
            Ok(_) => panic!("expected DegeneratedMatrixError"),
            Err(e) => {
                assert!(matches!(
                    e,
                    DegeneratedMatrixError {
                        number_of_rows: 0,
                        number_of_columns: 0
                    }
                ));
                assert_str_eq!(
                    format!("{e:?}"),
                    "DegeneratedMatrixError { number_of_rows: 0, number_of_columns: 0 }"
                );
                assert_str_eq!(format!("{e}"), "The resulting matrix is degenerated (0x0)");
            }
        }

        assert_eq!(
            matrix_a
                .to_submatrix(
                    &ClosureDimensionFilter::new(|_| true),
                    &ClosureDimensionFilter::new(|_| true),
                )
                .unwrap(),
            matrix_a,
        );

        let mut sub_bm = CSVBinaryMatrix {
            number_of_rows: 2,
            number_of_columns: 2,
            distances: vec![0, 1, 2, 0],
        };
        assert_eq!(
            matrix_a
                .to_submatrix(
                    &ClosureDimensionFilter::new(|i| { i != 1 }),
                    &ClosureDimensionFilter::new(|j| { j != 2 }),
                )
                .unwrap(),
            sub_bm,
        );

        sub_bm = CSVBinaryMatrix {
            number_of_rows: 2,
            number_of_columns: 2,
            distances: vec![0, 1, 2],
        };
        assert_eq!(
            matrix_a
                .to_submatrix(
                    &ClosureDimensionFilter::new(|i| { i != 2 }),
                    &ClosureDimensionFilter::new(|j| { j != 0 })
                )
                .unwrap(),
            sub_bm
        );

        sub_bm = CSVBinaryMatrix {
            number_of_rows: 1,
            number_of_columns: 3,
            distances: vec![1, 1],
        };
        assert_eq!(
            matrix_a
                .to_submatrix(
                    &ClosureDimensionFilter::new(|i| { i > 1 }),
                    &ClosureDimensionFilter::new(|_| true)
                )
                .unwrap(),
            sub_bm
        );

        sub_bm = CSVBinaryMatrix {
            number_of_rows: 2,
            number_of_columns: 2,
            distances: vec![0, 1, 1, 1],
        };
        assert_eq!(
            CSVBinaryMatrix {
                number_of_rows: 2,
                number_of_columns: 3,
                distances: vec![0, 1, 1, 2, 1],
            }
            .to_submatrix(
                &ClosureDimensionFilter::new(|_| true),
                &ClosureDimensionFilter::new(|j| { j != 0 })
            )
            .unwrap(),
            sub_bm
        );

        sub_bm = CSVBinaryMatrix {
            number_of_rows: 2,
            number_of_columns: 2,
            distances: vec![0, 1, 2, 0],
        };
        assert_eq!(
            bin_csv_nullrowcol
                .to_submatrix(
                    &ClosureDimensionFilter::new(|i| { i != 1 }),
                    &ClosureDimensionFilter::new(|j| { j != 2 })
                )
                .unwrap(),
            sub_bm
        );
    }

    #[rstest]
    fn into_reversed_submatrices(
        zeros_matrix: CSVBinaryMatrix,
        matrix_a: CSVBinaryMatrix,
        bin_csv_nullrowcol: CSVBinaryMatrix,
    ) {
        assert_eq!(
            zeros_matrix.clone().into_reversed_submatrices(vec![
                (
                    &ClosureDimensionFilter::new(|_| true),
                    &ClosureDimensionFilter::new(|_| true)
                ),
                (
                    &ClosureDimensionFilter::new(|_| false),
                    &ClosureDimensionFilter::new(|_| false)
                ),
            ],),
            vec![
                Ok(zeros_matrix),
                Err(DegeneratedMatrixError {
                    number_of_rows: 0,
                    number_of_columns: 0
                })
            ]
        );

        assert_eq!(
            matrix_a.clone().into_reversed_submatrices(vec![(
                &ClosureDimensionFilter::new(|_| true),
                &ClosureDimensionFilter::new(|_| true)
            ),],),
            vec![Ok(CSVBinaryMatrix {
                number_of_rows: 3,
                number_of_columns: 3,
                distances: vec![1, 4, 1, 1, 1, 0],
            })]
        );

        assert_eq!(
            matrix_a.clone().into_reversed_submatrices(vec![(
                &ClosureDimensionFilter::new(|i| { i != 1 }),
                &ClosureDimensionFilter::new(|j| { j != 2 })
            )],),
            vec![Ok(CSVBinaryMatrix {
                number_of_rows: 2,
                number_of_columns: 2,
                distances: vec![0, 2, 1, 0],
            })]
        );

        assert_eq!(
            matrix_a.clone().into_reversed_submatrices(vec![(
                &ClosureDimensionFilter::new(|i| { i != 2 }),
                &ClosureDimensionFilter::new(|j| { j != 0 })
            ),]),
            vec![Ok(CSVBinaryMatrix {
                number_of_rows: 2,
                number_of_columns: 2,
                distances: vec![2, 1, 0],
            })]
        );

        assert_eq!(
            matrix_a.clone().into_reversed_submatrices(vec![(
                &ClosureDimensionFilter::new(|i| { i > 1 }),
                &ClosureDimensionFilter::new(|_| true)
            ),],),
            vec![Ok(CSVBinaryMatrix {
                number_of_rows: 1,
                number_of_columns: 3,
                distances: vec![1, 1],
            })],
        );

        assert_eq!(
            bin_csv_nullrowcol.clone().into_reversed_submatrices(vec![(
                &ClosureDimensionFilter::new(|i| { i != 1 }),
                &ClosureDimensionFilter::new(|j| { j != 2 })
            ),],),
            vec![Ok(CSVBinaryMatrix {
                number_of_rows: 2,
                number_of_columns: 2,
                distances: vec![0, 2, 1, 0],
            })],
        );
    }
}
