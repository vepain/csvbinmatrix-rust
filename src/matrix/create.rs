#![doc = include_str!("../../docs/matrix/create.md")]

use thiserror::Error;

use super::items::Coordinates;
use super::iter::OnesCoordinatesColumnCursor;
use super::CSVBinaryMatrix;

impl std::default::Default for CSVBinaryMatrix {
    /// Creates a new empty or all-zeros [`CSVBinaryMatrix`].
    ///
    /// # Example
    ///
    /// ```rust
    /// use csvbinmatrix::prelude::CSVBinaryMatrix;
    ///
    /// assert_eq!(
    ///     CSVBinaryMatrix::default(),
    ///     CSVBinaryMatrix::try_from(&vec![vec![0]]).unwrap()
    /// );
    /// ```
    fn default() -> Self {
        Self {
            number_of_rows: 1,
            number_of_columns: 1,
            distances: vec![0],
        }
    }
}

/// The coordinates exceed the number of rows.
#[derive(Error, Debug)]
#[error("Coordinates exceed the number of rows: {0} >= {1}")]
pub struct DistancesExceedRowsError(usize, usize);

/// Error when create a [`CSVBinaryMatrix`] from a list of coordinates.
#[derive(Error, Debug)]
pub enum TryFromCoordinatesError {
    /// The coordinates exceed the number of rows.
    #[error("Coordinates exceed the number of rows: {0} >= {1}")]
    ExceedRows(usize, usize),
    /// The coordinates exceed the number of columns.
    #[error("Coordinates exceed the number of columns: {0} >= {1}")]
    ExceedColumns(usize, usize),
    /// The coordinates are not sorted.
    #[error("The coordinates are not sorted: {0} >= {1}")]
    NotSorted(Coordinates, Coordinates),
}

impl CSVBinaryMatrix {
    /// Create a [`CSVBinaryMatrix`] from distances.
    ///
    /// The first distance is the one that separates the first cell from the first one.
    /// The last distance is the one that separates the penulti-last one from the last one.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// # let matrix = CSVBinaryMatrix::try_from(&vec![
    /// #     vec![0, 0, 0],
    /// #     vec![0, 0, 1],
    /// #     vec![0, 1, 1],
    /// #     vec![1, 1, 1],
    /// # ]).unwrap();
    /// match CSVBinaryMatrix::new(4, 3, vec![5, 2, 1, 1, 1, 1]) {
    ///     Ok(m) => assert_eq!(m, matrix),
    ///     Err(e) => panic!("[ERROR] {e}"),
    /// }
    /// ```
    ///
    /// # Errors
    ///
    /// Raises [`DistancesExceedRowsError`].
    ///
    pub fn new<I: IntoIterator<Item = usize>>(
        number_of_rows: usize,
        number_of_columns: usize,
        distances: I,
    ) -> Result<Self, DistancesExceedRowsError> {
        let mut distances_ok: Vec<usize> = Vec::new();
        let mut coordinates = Coordinates::new(0, 0);
        let mut ones_coordinates_column_cursor =
            OnesCoordinatesColumnCursor::new(number_of_columns, coordinates.clone());
        for distance in distances {
            coordinates = ones_coordinates_column_cursor.forward(distance);
            if coordinates.row() >= number_of_rows {
                return Err(DistancesExceedRowsError(coordinates.row(), number_of_rows));
            }
            distances_ok.push(distance);
        }
        distances_ok.push(
            (number_of_rows - 1 - coordinates.row()) * number_of_columns
                + (number_of_columns - 1 - coordinates.column()),
        );
        Ok(Self {
            number_of_rows,
            number_of_columns,
            distances: distances_ok,
        })
    }

    /// Create a [`CSVBinaryMatrix`] from the row-column coordinates.
    ///
    /// The coordinates must be sorted according to the row and then to the column.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// # let matrix = CSVBinaryMatrix::try_from(&vec![
    /// #     vec![0, 0, 0],
    /// #     vec![0, 0, 1],
    /// #     vec![0, 1, 1],
    /// #     vec![1, 1, 1],
    /// # ]).unwrap();
    /// match CSVBinaryMatrix::try_from_coordinates(
    ///     4,
    ///     3,
    ///     vec![(1, 2), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2)]
    /// ) {
    ///     Ok(matrix_from_coo) => assert_eq!(matrix, matrix_from_coo),
    ///     Err(e) => panic!("[ERROR] {e}"),
    /// }
    /// ```
    ///
    /// # Errors
    ///
    /// Raise a [`TryFromCoordinatesError`].
    ///
    pub fn try_from_coordinates<T: IntoIterator<Item = C>, C: Into<Coordinates>>(
        number_of_rows: usize,
        number_of_columns: usize,
        coordinates: T,
    ) -> Result<Self, TryFromCoordinatesError> {
        let mut distances = Vec::new();
        let mut distance: usize;
        let mut prev_coordinates = Coordinates::new(0, 0);
        let mut coordinates_iter = coordinates.into_iter();
        prev_coordinates = match coordinates_iter.next().map(std::convert::Into::into) {
            Some(current_coordinate) => {
                if current_coordinate.row() >= number_of_rows {
                    return Err(TryFromCoordinatesError::ExceedRows(
                        current_coordinate.row(),
                        number_of_rows,
                    ));
                }
                if current_coordinate.column() >= number_of_columns {
                    return Err(TryFromCoordinatesError::ExceedColumns(
                        current_coordinate.column(),
                        number_of_columns,
                    ));
                }
                distance = (current_coordinate.row() - prev_coordinates.row()) * number_of_columns
                    + current_coordinate.column()
                    - prev_coordinates.column();
                distances.push(distance);
                current_coordinate
            }
            None => Coordinates::new(0, 0),
        };
        for current_coordinate in coordinates_iter.map(std::convert::Into::into) {
            if current_coordinate <= prev_coordinates {
                return Err(TryFromCoordinatesError::NotSorted(
                    prev_coordinates,
                    current_coordinate,
                ));
            }
            if current_coordinate.row() >= number_of_rows {
                return Err(TryFromCoordinatesError::ExceedRows(
                    current_coordinate.row(),
                    number_of_rows,
                ));
            }
            if current_coordinate.column() >= number_of_columns {
                return Err(TryFromCoordinatesError::ExceedColumns(
                    current_coordinate.column(),
                    number_of_columns,
                ));
            }
            distance = (current_coordinate.row() - prev_coordinates.row()) * number_of_columns;
            if current_coordinate.column() < prev_coordinates.column() {
                distance -= prev_coordinates.column() - current_coordinate.column();
            } else {
                distance += current_coordinate.column() - prev_coordinates.column();
            }
            distances.push(distance);
            prev_coordinates = current_coordinate;
        }
        distance = (number_of_rows - 1 - prev_coordinates.row()) * number_of_columns
            + (number_of_columns - 1 - prev_coordinates.column());
        distances.push(distance);
        Ok(Self {
            number_of_rows,
            number_of_columns,
            distances,
        })
    }
}

/// Error when creating a [`CSVBinaryMatrix`] from boolean rows.
#[derive(Error, Debug)]
pub enum TryFromBoolRowsError {
    /// The number of columns in the current row is not equal to the number of columns in the first row.
    #[error("The number of columns in row {row_index} is not equal to the number of columns in the first row: {current_number_of_columns} != {expected_number_of_columns}")]
    NumberOfColumnsMismatch {
        /// The row index
        row_index: usize,
        /// The number of columns in the current row
        current_number_of_columns: usize,
        /// The number of columns in the first row
        expected_number_of_columns: usize,
    },
}

impl TryFrom<&Vec<Vec<bool>>> for CSVBinaryMatrix {
    type Error = TryFromBoolRowsError;
    /// Create a [`CSVBinaryMatrix`] from boolean rows.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// let matrix = match CSVBinaryMatrix::try_from(&vec![
    ///     vec![false, false, false],
    ///     vec![false, false, true],
    ///     vec![false, true, true],
    ///     vec![true, true, true],
    /// ]) {
    ///     Ok(matrix) => matrix,
    ///     Err(e) => panic!("[ERROR] {e}"),
    /// };
    /// ```
    ///
    /// # Errors
    ///
    /// Raise a [`TryFromBoolRowsError`].
    ///
    fn try_from(rows: &Vec<Vec<bool>>) -> Result<Self, Self::Error> {
        let number_of_rows = rows.len();
        let number_of_columns = rows[0].len();
        let mut distances = Vec::new();
        let mut distance = 0;
        for (row_index, row) in rows.iter().enumerate() {
            if row.len() != number_of_columns {
                return Err(Self::Error::NumberOfColumnsMismatch {
                    row_index,
                    current_number_of_columns: row.len(),
                    expected_number_of_columns: number_of_columns,
                });
            }
            for &cell in row {
                if cell {
                    distances.push(distance);
                    distance = 1;
                } else {
                    distance += 1;
                }
            }
        }
        distances.push(distance - 1);
        Ok(Self {
            number_of_rows,
            number_of_columns,
            distances,
        })
    }
}

/// Error when creating a [`CSVBinaryMatrix`] from u8 rows.
#[derive(Error, Debug)]
pub enum TryFromIntRowsError {
    /// The number of columns in the current row is not equal to the number of columns in the first row.
    #[error("The number of columns in row {row_index} is not equal to the number of columns in the first row: {current_number_of_columns} != {expected_number_of_columns}")]
    NumberOfColumnsMismatch {
        /// The row index
        row_index: usize,
        /// The number of columns in the current row
        current_number_of_columns: usize,
        /// The number of columns in the first row
        expected_number_of_columns: usize,
    },
    /// The value in the row is not 0 or 1.
    #[error("The value in row {row_index} is not 0 or 1: {unexpected_value}")]
    RowValueNot0or1 {
        /// The row index
        row_index: usize,
        /// The unexpected value
        unexpected_value: u8,
    },
}

impl TryFrom<&Vec<Vec<u8>>> for CSVBinaryMatrix {
    type Error = TryFromIntRowsError;

    /// Create a [`CSVBinaryMatrix`] from 0-1 rows.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// let matrix = match CSVBinaryMatrix::try_from(&vec![
    ///     vec![0, 0, 0],
    ///     vec![0, 0, 1],
    ///     vec![0, 1, 1],
    ///     vec![1, 1, 1],
    /// ]) {
    ///     Ok(matrix) => matrix,
    ///     Err(e) => panic!("[ERROR] {e}"),
    /// };
    /// ```
    ///
    /// # Errors
    ///
    /// Raise a [`TryFromIntRowsError`].
    ///
    fn try_from(rows: &Vec<Vec<u8>>) -> Result<Self, Self::Error> {
        let number_of_rows = rows.len();
        let number_of_columns = rows[0].len();
        let mut distances = Vec::new();
        let mut distance = 0;
        for (row_index, row) in rows.iter().enumerate() {
            if row.len() != number_of_columns {
                return Err(Self::Error::NumberOfColumnsMismatch {
                    row_index,
                    current_number_of_columns: row.len(),
                    expected_number_of_columns: number_of_columns,
                });
            }
            for &cell in row {
                match cell {
                    0 => distance += 1,
                    1 => {
                        distances.push(distance);
                        distance = 1;
                    }
                    x => {
                        return Err(Self::Error::RowValueNot0or1 {
                            row_index,
                            unexpected_value: x,
                        })
                    }
                }
            }
        }
        distances.push(distance - 1);
        Ok(Self {
            number_of_rows,
            number_of_columns,
            distances,
        })
    }
}

impl<const N: usize, const M: usize> TryFrom<&[[u8; M]; N]> for CSVBinaryMatrix {
    type Error = TryFromIntRowsError;

    /// Create a [`CSVBinaryMatrix`] from 0-1 rows.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use csvbinmatrix::prelude::CSVBinaryMatrix;
    /// #
    /// let matrix = match CSVBinaryMatrix::try_from(&[
    ///     [0, 0, 0],
    ///     [0, 0, 1],
    ///     [0, 1, 1],
    ///     [1, 1, 1],
    /// ]) {
    ///     Ok(matrix) => matrix,
    ///     Err(e) => panic!("[ERROR] {e}"),
    /// };
    /// ```
    ///
    /// # Errors
    ///
    /// Raise a [`TryFromIntRowsError`].
    ///
    fn try_from(rows: &[[u8; M]; N]) -> Result<Self, Self::Error> {
        let number_of_rows = rows.len();
        let number_of_columns = rows[0].len();
        let mut distances = Vec::new();
        let mut distance = 0;
        for (row_index, row) in rows.iter().enumerate() {
            for &cell in row {
                match cell {
                    0 => distance += 1,
                    1 => {
                        distances.push(distance);
                        distance = 1;
                    }
                    x => {
                        return Err(Self::Error::RowValueNot0or1 {
                            row_index,
                            unexpected_value: x,
                        })
                    }
                }
            }
        }
        distances.push(distance - 1);
        Ok(Self {
            number_of_rows,
            number_of_columns,
            distances,
        })
    }
}

#[cfg(test)]
mod tests {
    mod csvbm_impl {
        use std::error::Error;

        use super::super::super::tests::{matrix_a, zeros_matrix};
        use super::super::super::CSVBinaryMatrix;
        use crate::matrix::items::Coordinates;
        use pretty_assertions::assert_eq;
        use rstest::rstest;

        #[rstest]
        fn default() {
            assert_eq!(
                CSVBinaryMatrix::default(),
                CSVBinaryMatrix {
                    number_of_rows: 1,
                    number_of_columns: 1,
                    distances: vec![0],
                }
            );
        }

        #[rstest]
        fn new(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
            assert_eq!(CSVBinaryMatrix::new(3, 3, vec![]).unwrap(), zeros_matrix);
            assert_eq!(
                CSVBinaryMatrix::new(3, 3, vec![0, 1, 1, 1, 4]).unwrap(),
                matrix_a
            );
        }

        #[rstest]
        fn new_coordinates_exceed_rows_error() {
            match CSVBinaryMatrix::new(2, 3, vec![0, 1, 1, 1, 6]) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "DistancesExceedRowsError(3, 2)");
                    assert!(e.source().is_none());
                    assert_eq!(
                        format!("{e}"),
                        "Coordinates exceed the number of rows: 3 >= 2"
                    );
                }
            }
        }

        #[rstest]
        fn try_from_coordinates(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
            assert_eq!(
                CSVBinaryMatrix::try_from_coordinates::<Vec<Coordinates>, Coordinates>(
                    3,
                    3,
                    vec![]
                )
                .unwrap(),
                zeros_matrix
            );
            assert_eq!(
                CSVBinaryMatrix::try_from_coordinates(
                    3,
                    3,
                    vec![(0, 0), (0, 1), (0, 2), (1, 0), (2, 1)]
                )
                .unwrap(),
                matrix_a
            );
        }

        #[rstest]
        fn try_from_coordinates_exceed_rows_error() {
            match CSVBinaryMatrix::try_from_coordinates(
                2,
                3,
                vec![(2, 0), (0, 1), (0, 2), (1, 0), (2, 1)],
            ) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "ExceedRows(2, 2)");
                    assert_eq!(
                        format!("{e}"),
                        "Coordinates exceed the number of rows: 2 >= 2"
                    );
                }
            }
            match CSVBinaryMatrix::try_from_coordinates(
                2,
                3,
                vec![(0, 0), (0, 1), (0, 2), (1, 0), (2, 1)],
            ) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "ExceedRows(2, 2)");
                    assert_eq!(
                        format!("{e}"),
                        "Coordinates exceed the number of rows: 2 >= 2"
                    );
                }
            }
        }

        #[rstest]
        fn try_from_coordinates_coordinates_exceed_columns_error() {
            match CSVBinaryMatrix::try_from_coordinates(
                2,
                3,
                vec![(0, 3), (0, 1), (0, 2), (1, 0), (2, 1)],
            ) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "ExceedColumns(3, 3)");
                    assert_eq!(
                        format!("{e}"),
                        "Coordinates exceed the number of columns: 3 >= 3"
                    );
                }
            }
            match CSVBinaryMatrix::try_from_coordinates(
                2,
                3,
                vec![(0, 0), (0, 1), (0, 3), (1, 0), (2, 1)],
            ) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "ExceedColumns(3, 3)");
                    assert_eq!(
                        format!("{e}"),
                        "Coordinates exceed the number of columns: 3 >= 3"
                    );
                }
            }
        }

        #[rstest]
        fn try_from_coordinates_not_sorted_error() {
            match CSVBinaryMatrix::try_from_coordinates(
                2,
                3,
                vec![(0, 0), (0, 1), (0, 2), (1, 1), (1, 0)],
            ) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(format!("{e:?}"), "NotSorted(Coordinates { row: 1, column: 1 }, Coordinates { row: 1, column: 0 })");
                    assert_eq!(
                        format!("{e}"),
                        "The coordinates are not sorted: (1, 1) >= (1, 0)"
                    );
                }
            }
        }
    }

    mod try_from_bool_rows {
        use super::super::super::tests::{matrix_a, zeros_matrix};
        use super::super::super::CSVBinaryMatrix;
        use pretty_assertions::assert_eq;
        use rstest::rstest;

        #[rstest]
        fn try_from_boolean_rows(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
            assert_eq!(
                CSVBinaryMatrix::try_from(&vec![
                    vec![false, false, false],
                    vec![false, false, false],
                    vec![false, false, false],
                ])
                .unwrap(),
                zeros_matrix
            );
            assert_eq!(
                CSVBinaryMatrix::try_from(&vec![
                    vec![true, true, true],
                    vec![true, false, false],
                    vec![false, true, false],
                ])
                .unwrap(),
                matrix_a
            );
        }

        #[rstest]
        fn try_from_bool_columns_mismatch_error() {
            match CSVBinaryMatrix::try_from(&vec![
                vec![true, true, true],
                vec![true, false, false, true],
                vec![false, true, false],
            ]) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert_eq!(
                        format!("{e:?}"),
                        "NumberOfColumnsMismatch { row_index: 1, current_number_of_columns: 4, expected_number_of_columns: 3 }"
                    );
                    assert_eq!(format!("{e}"), "The number of columns in row 1 is not equal to the number of columns in the first row: 4 != 3");
                }
            }
        }
    }

    mod try_from_u8_vectors {
        use super::super::super::tests::{matrix_a, zeros_matrix};
        use super::super::CSVBinaryMatrix;
        use super::super::TryFromIntRowsError;
        use pretty_assertions::assert_eq;
        use rstest::rstest;

        #[rstest]
        fn try_from_u8_vec(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
            assert_eq!(
                CSVBinaryMatrix::try_from(&vec![vec![0, 0, 0], vec![0, 0, 0], vec![0, 0, 0],])
                    .unwrap(),
                zeros_matrix
            );
            assert_eq!(
                CSVBinaryMatrix::try_from(&vec![vec![1, 1, 1], vec![1, 0, 0], vec![0, 1, 0],])
                    .unwrap(),
                matrix_a
            );
        }

        #[rstest]
        fn try_from_invalid_u8_vec() {
            match CSVBinaryMatrix::try_from(&vec![vec![1, 1], vec![1, 3], vec![0, 1]]) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert!(matches!(
                        e,
                        TryFromIntRowsError::RowValueNot0or1 {
                            row_index: 1,
                            unexpected_value: 3
                        }
                    ));
                    assert_eq!(
                        format!("{e:?}"),
                        "RowValueNot0or1 { row_index: 1, unexpected_value: 3 }"
                    );
                    assert_eq!(format!("{e}"), "The value in row 1 is not 0 or 1: 3");
                }
            }
        }

        #[rstest]
        fn try_from_u8_columns_mismatch_error() {
            match CSVBinaryMatrix::try_from(&vec![vec![1, 1, 1], vec![1, 0, 0, 1], vec![0, 1, 0]]) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert!(matches!(
                        e,
                        TryFromIntRowsError::NumberOfColumnsMismatch {
                            row_index: 1,
                            current_number_of_columns: 4,
                            expected_number_of_columns: 3
                        }
                    ));
                    assert_eq!(
                        format!("{e:?}"),
                        "NumberOfColumnsMismatch { row_index: 1, current_number_of_columns: 4, expected_number_of_columns: 3 }"
                    );
                    assert_eq!(format!("{e}"), "The number of columns in row 1 is not equal to the number of columns in the first row: 4 != 3");
                }
            }
        }
    }

    mod try_from_u8_arrays {
        use super::super::super::tests::{matrix_a, zeros_matrix};
        use super::super::CSVBinaryMatrix;
        use super::super::TryFromIntRowsError;
        use pretty_assertions::assert_eq;
        use rstest::rstest;

        #[rstest]
        fn try_from_u8_arrays(zeros_matrix: CSVBinaryMatrix, matrix_a: CSVBinaryMatrix) {
            assert_eq!(
                CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 0], [0, 0, 0],]).unwrap(),
                zeros_matrix
            );
            assert_eq!(
                CSVBinaryMatrix::try_from(&[[1, 1, 1], [1, 0, 0], [0, 1, 0],]).unwrap(),
                matrix_a
            );
        }

        #[rstest]
        fn try_from_invalid_u8() {
            match CSVBinaryMatrix::try_from(&[[1, 1], [1, 3], [0, 1]]) {
                Ok(_) => panic!("Expected error"),
                Err(e) => {
                    assert!(matches!(
                        e,
                        TryFromIntRowsError::RowValueNot0or1 {
                            row_index: 1,
                            unexpected_value: 3
                        }
                    ));
                    assert_eq!(format!("{e}"), "The value in row 1 is not 0 or 1: 3");
                }
            }
        }
    }
}
