use rstest::rstest;

#[rstest]
fn quick_usage() -> Result<(), Box<dyn std::error::Error>> {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let matrix = match CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]]) {
        Ok(m) => m,
        Err(e) => panic!("[ERROR] Cannot create the matrix: {e}"),
    };

    println!("Matrix stats");
    println!("------------");
    println!(
        "Dimensions: {}x{} ({})",
        matrix.number_of_rows(),
        matrix.number_of_columns(),
        matrix.number_of_cells()
    );
    println!(
        "Number of ones/zeros: {}/{}",
        matrix.number_of_ones(),
        matrix.number_of_zeros()
    );
    println!("Density: {:.2}%", matrix.density() * 100.0);
    println!("Sparsity: {:.2}%", matrix.sparsity() * 100.0);
    println!();

    println!("Coordinates of ones");
    println!("-------------------");
    println!("row\tcolumn");
    println!("---\t-------");
    for coordinates in matrix.iter_ones_coordinates() {
        println!("{}\t{}", coordinates.row(), coordinates.column());
    }
    println!();

    match matrix.to_file("mymatrix.csvbm") {
        Ok(_) => println!("[INFO] File created"),
        Err(e) => println!("[ERROR] Creating the file fails: {e}"),
    }

    match CSVBinaryMatrix::try_from_file("mymatrix.csvbm") {
        Ok(m) => {
            println!("[INFO] Read from file");
            assert_eq!(m, matrix)
        }
        Err(e) => println!("[ERROR] Cannot read the file: {e}"),
    }

    std::fs::remove_file("mymatrix.csvbm").unwrap_or(());
    Ok(())
}
