use rstest::{fixture, rstest};

#[fixture]
#[allow(clippy::let_and_return)]
fn matrix() -> csvbinmatrix::prelude::CSVBinaryMatrix {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let matrix = CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]]).unwrap();

    matrix
}

#[rstest]
fn try_from_u8_rows(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    match CSVBinaryMatrix::try_from(&vec![
        vec![0, 0, 0],
        vec![0, 0, 1],
        vec![0, 1, 1],
        vec![1, 1, 1],
    ]) {
        Ok(matrix_from_u8_vectors) => assert_eq!(matrix_from_u8_vectors, matrix),
        Err(e) => panic!("[ERROR] {e}"),
    };
}

#[rstest]
fn try_from_bool_rows(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    match CSVBinaryMatrix::try_from(&vec![
        vec![false, false, false],
        vec![false, false, true],
        vec![false, true, true],
        vec![true, true, true],
    ]) {
        Ok(matrix_from_booleans) => assert_eq!(matrix_from_booleans, matrix),
        Err(e) => panic!("[ERROR] {e}"),
    }
}

#[rstest]
fn try_from_usize_coordinates(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    match CSVBinaryMatrix::try_from_coordinates(
        4,
        3,
        vec![(1, 2), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2)],
    ) {
        Ok(matrix_from_coordinates) => assert_eq!(matrix_from_coordinates, matrix),
        Err(e) => panic!("[ERROR] {e}"),
    }
}

#[rstest]
fn try_from_coordinates(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::{CSVBinaryMatrix, Coordinates};

    match CSVBinaryMatrix::try_from_coordinates(
        4,
        3,
        vec![
            Coordinates::new(1, 2),
            Coordinates::new(2, 1),
            Coordinates::new(2, 2),
            Coordinates::new(3, 0),
            Coordinates::new(3, 1),
            Coordinates::new(3, 2),
        ],
    ) {
        Ok(matrix_from_coordinates) => assert_eq!(matrix_from_coordinates, matrix),
        Err(e) => panic!("[ERROR] {e}"),
    }
}

#[rstest]
fn new(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    match CSVBinaryMatrix::new(4, 3, vec![5, 2, 1, 1, 1, 1]) {
        Ok(m) => assert_eq!(m, matrix),
        Err(e) => panic!("[ERROR] {e}"),
    }
}
