use rstest::rstest;

#[rstest]
fn with_bool_vector_and_closure() {
    use csvbinmatrix::prelude::{
        BoolVecDimensionFilter, CSVBinaryMatrix, ClosureDimensionFilter, DimensionFilter,
    };

    let matrix = CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]]).unwrap();

    // Filter with a boolean vector:
    // * to represent complex truth states
    // * costly
    let row_filter =
        match BoolVecDimensionFilter::new(vec![true, false, true, true], matrix.number_of_rows()) {
            Ok(filter) => filter,
            Err(err) => panic!("[ERROR] {err}"),
        };

    // Filter with a closure
    // * to represent simple truth states
    // * efficient
    let column_filter = ClosureDimensionFilter::new(|j| j != 0);

    assert!(row_filter.accepts(0));
    assert!(!column_filter.accepts(0));

    // You can drop the row filter and take the ownership of the boolean vectors.
    let boolvec_row = row_filter.into_boolean_vector();
    println!("Boolean vector for the rows: {boolvec_row:?}");
}
