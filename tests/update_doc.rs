use rstest::{fixture, rstest};

#[fixture]
#[allow(clippy::let_and_return)]
fn matrix() -> csvbinmatrix::prelude::CSVBinaryMatrix {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let matrix = CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]]).unwrap();

    matrix
}

#[rstest]
fn extend_with_rows(mut matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let extension = CSVBinaryMatrix::try_from(&[[0, 1, 0]]).unwrap();

    match matrix.extend_with_rows(extension) {
        Ok(()) => (),
        Err(err) => panic!("[ERROR] {err}"),
    }
    assert_eq!(
        matrix,
        CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1], [0, 1, 0]])
            .unwrap()
    );
}

#[rstest]
fn extend_with_columns(mut matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let extension = CSVBinaryMatrix::try_from(&[[1], [1], [1], [1]]).unwrap();

    match matrix.extend_with_columns(extension) {
        Ok(()) => {}
        Err(err) => panic!("[ERROR] {err}"),
    }
    assert_eq!(
        matrix,
        CSVBinaryMatrix::try_from(&[[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 1, 1, 1],])
            .unwrap()
    );
}
