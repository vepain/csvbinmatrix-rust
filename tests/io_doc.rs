use rstest::{fixture, rstest};

#[fixture]
fn matrix() -> csvbinmatrix::prelude::CSVBinaryMatrix {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    CSVBinaryMatrix::try_from(&[[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 1, 1]]).unwrap()
}

#[rstest]
fn without_consuming(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    match matrix.to_file("mymatrix.csvbm") {
        Ok(_) => println!("[INFO] File created"),
        Err(e) => println!("[ERROR] creating file fails: {e}"),
    }
    std::fs::remove_file("mymatrix.csvbm").unwrap_or(());

    // You can still use `matrix`
    assert_eq!(matrix.number_of_ones(), 6);
}

#[rstest]
fn with_consuming(matrix: csvbinmatrix::prelude::CSVBinaryMatrix) {
    use csvbinmatrix::prelude::CSVBinaryMatrix;

    let expected_matrix_from_file = matrix.clone();

    match matrix.into_file("mymatrix.csvbm") {
        Ok(_) => println!("[INFO] File created"),
        Err(e) => println!("[ERROR] creating file fails: {e}"),
    }

    // `matrix` is consumed so you cannot use it anymore.

    let matrix_from_file = match CSVBinaryMatrix::try_from_file("mymatrix.csvbm") {
        Ok(m) => m,
        Err(e) => panic!("[ERROR] reading file fails: {e}"),
    };

    assert_eq!(matrix_from_file, expected_matrix_from_file);
    std::fs::remove_file("mymatrix.csvbm").unwrap_or(());
}
