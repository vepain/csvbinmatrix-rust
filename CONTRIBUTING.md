# Contributing

## Code guidelines

* [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html)


## Git workflow

I try to follow this [successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/).

### Commit

Please use [conventional commit messages](https://www.conventionalcommits.org/en/v1.0.0/).

VSCode extension `vivaxy.vscode-conventional-commits` can help you to write a conventional commit message.


## Push a new release to crates.io

Please use [cargo-release](https://github.com/crate-ci/cargo-release) to push a new release to [crates.io](https://crates.io/).

From the `develop` branch:
```sh
cargo release patch  # to test and verify what will automaticaly change
```
When you have verified all the changes and fix the `cargo release` errors:
```sh
cargo release --execute
```

Then to respect [the Git workflow](#git-workflow):
```sh
# You are still on the `develop` branch
git checkout main
git merge --no-ff v0.1.0  # if the created tag is `v0.1.0`
git push origin main --follow-tags
```


## Testing

### Install dependencies

* [cargo-nextest](https://nexte.st/)
* [cargo-llvm-cov](https://crates.io/crates/cargo-llvm-cov)
    ```bash
    cargo install cargo-llvm-cov
    ```

### Generate report on terminal

At the workspace root:
```bash
cargo nextest run
```

### Generating coverage

See at the beginning of the bellow script the packages to install.

At the workspace root:

```bash
./coverage.sh
cargo llvm-cov --open # open HTML report into the navigator
```

It also generates a `.lcov.info` lcov file.