# Change log

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> 'M', 'm' and 'p' are respectively corresponding to major, minor and patch

<!-- The order of keywords:
## [0.2.0] - 2024-04-20

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
-->

<!-- next-header -->

## [Unreleased] - ReleaseDate

## [0.7.0] - 2024-05-03

### Added

* Implement `TryFrom` u8 arrays for `CSVBinaryMatrix`
* `CSVBinaryMatrix::extend_with_columns` method
* Tutorial on updating the matrix

### Changed

* **Breaking change:** Rename `CSVBinaryMatrix::from_file` to `CSVBinaryMatrix::try_from_file`
* **Breaking change:** Change `update::DimensionMismatchError` to enum `UpdateMatrixError`
* **Breaking change:** Rename `CSVBinaryMatrix::append_rows` to `CSVBinaryMatrix::extend_with_rows`

## [0.6.0] - 2024-05-01

### Added

* Enums of errors in `csvbinmatrix::matrix::io` module
* Errors in `csvbinmatrix::matrix::create` module
* Implement `TryFrom` boolean and u8 vectors of vectors for `CSVBinMatrix`
* Implement `PartialOrd` for `Coordinates`
* `csvbinmatrix::matrix::update::DimensionMismatchError` error
* `csvbinmatrix::matrix::views::DegeneratedMatrixError` error

### Changed

* **Breaking changes:** Rename `BinCSV` to `CSVBinaryMatrix`
* **Breaking changes:** Rename `csvbinmatrix::bincsv` to `csvbinmatrix::matrix`
* **Breaking changes:** Rename symbols to follow the Rust API Guidelines
    * `CSVBinaryMatrix::write_to_file` -> `CSVBinaryMatrix::to_file`
    * `CSVBinaryMatrix::to_file` -> `CSVBinaryMatrix::into_file`
    * `CSVBinaryMatrix::read_from_file` -> `CSVBinaryMatrix::from_file`
    * `OnesCoordinatesIter` -> `IterOnesCoordinates`
    * `OnesCoordinatesReverseIter` -> `IterReversedOnesCoordinates`
    * `CSVBinaryMatrix::to_reversed` -> `CSVBinaryMatrix::into_reversed`
    * `CSVBinaryMatrix::submatrix` -> `CSVBinaryMatrix::to_submatrix`
    * `CSVBinaryMatrix::to_reversed_sub_binmatrices` -> `CSVBinaryMatrix::into_reversed_submatrices`
    * `BoolVecDimensionFilter::to_boolean_vector` -> `BoolVecDimensionFilter::into_boolean_vector`
* **Breaking changes:** `CSVBinaryMatrix::new` now takes an iterable of distances and returns a `Result`
* **Breaking changes:** The CSVBM file format store a boolean to know if the matrix has been reversed (especially with the `CSVBinaryMatrix::into_file` method)
* **Breaking changes:** `CSVBinaryMatrix::from_file` now returns custom errors
* **Breaking changes:** Rename `DimensionFilter::filter` to `DimensionFilter::accepts`
* **Breaking changes:** `BoolVecDimensionFilter::new` now returns a `Result`
* **Breaking changes:** `CSBinaryMatrix::append_rows` now returns a `Result`
* **Breaking changes:** `CSVBinaryMatrix::to_submatrix` and `CSVBinaryMatrix::into_reversed_submatrices` now return a `Result`
* Documentation and tutorials

### Removed

* **Breaking changes:** `CSVBinaryMatrix::from_u8_rows` and `CSVBinaryMatrix::from_boolean_rows` (implement `TryFrom` instead)
* **Breaking changes:** `csvbinmatrix::matrix::DegeneratedBinMatrix`

## [0.5.1] - 2024-04-27

### Added

* `csvbinmatrix::prelude` module

### Changed

* Refactor code by separating main structures from their iteration logics

## [0.5.0] - 2024-04-26

### Added

* `BinCSV::number_of_cells` method
* `BinCSV::sparsity` method
* `BinCSV::density` method

## [0.4.0] - 2024-04-26

### Added

* `BinCSV::to_reversed` method

### Changed

* Documentation on submatrices

## [0.3.0] - 2024-04-26

### Added

* `crate::bincsv::Coordinates` structure for row and column coordinates
* `crate::filter` module for row and column filters
* `crate::bincsv::DegeneratedBinMatrix` structure for degenerated matrices
* `crate::bincsv::SubBinCSV` enum for submatrix functions results
* `BinCSV::submatrix` method
* `BinCSV:reverse` method
* `BinCSV::from_u8_rows` method
* `BinCSV::to_file` method
* Documentation on submatrices

### Changed

* `BinCSV::from_reverse_bincsv` is renamed to `to_reversed_sub_binmatrices`
* `BinCSV::to_reversed_sub_binmatrices` now takes a vector of row and column filters and returns several submatrices
* `BinCSV::from_rows` becomes `BinCSV::from_boolean_rows`
* Use `Coordinates` and `OnesCoordinatesColumnCursor` to iterate or read next/previous ones' coordinates

## [0.2.1] - 2024-04-21

### Added

* Documentation
* Tests for `BinCSV::is_complete` and `BinCSV::is_empty` methods

## [0.2.0] - 2024-04-20

### Added

* New `BinCSV` methods:
    * `is_complete` and `is_empty`
    * `append_rows`
* Tests and documentation

### Fixed

* `BinCSV.new` returns empty distance list

## [0.1.1] - 2024-04-15

Publish `csvbinmatrix` package to [crates.io](https://crates.io/).

<!-- next-url -->
[Unreleased]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.7.0...HEAD
[0.7.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.5.1...v0.6.0
[0.5.1]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/vepain/csvbinmatrix-rust/-/compare/v0.1.0...v0.1.1