# Iterate over CSV Binary Matrix items

This module defines the core iteration logic for [`CSVBinaryMatrix`].
<!-- DOCU It includes the IntoIterator implementations on bit-arrays and their references, as well as the IntoIter struct that walks bit-arrays by value. -->

[`CSVBinaryMatrix`]: crate::matrix::CSVBinaryMatrix