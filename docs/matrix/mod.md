# Compressed Sparse Vector (CSV) Binary Matrix

[`CSVBinaryMatrix`] is a binary matrix represented by the Compressed Sparse Vector (CSV) format.

[`CSVBinaryMatrix`]: crate::matrix::CSVBinaryMatrix