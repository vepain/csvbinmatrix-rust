# Update CSV Binary Matrix

This module contains the update logic for [`CSVBinaryMatrix`].

[`CSVBinaryMatrix`]: crate::matrix::CSVBinaryMatrix

Because of the CSV format, extend with rows is trivial (O(1)) while append columns is more complex (in the number of ones in the resulting matrix).

## Examples

### Extend with rows

```rust
use csvbinmatrix::prelude::CSVBinaryMatrix;

let mut matrix = CSVBinaryMatrix::try_from(&[
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 1],
    [1, 1, 1],
]).unwrap();

let extension = CSVBinaryMatrix::try_from(&[[0, 1, 0]]).unwrap();

match matrix.extend_with_rows(extension) {
    Ok(()) => (),
    Err(err) => panic!("[ERROR] {err}"),
}
assert_eq!(
    matrix,
    CSVBinaryMatrix::try_from(&[
        [0, 0, 0],
        [0, 0, 1],
        [0, 1, 1],
        [1, 1, 1],
        [0, 1, 0]
    ]).unwrap()
);
```

### Extend with columns

```rust
use csvbinmatrix::prelude::CSVBinaryMatrix;

let mut matrix = CSVBinaryMatrix::try_from(&[
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 1],
    [1, 1, 1],
]).unwrap();

let extension = CSVBinaryMatrix::try_from(&[[1], [1], [1], [1]]).unwrap();

match matrix.extend_with_columns(extension) {
    Ok(()) => (),
    Err(err) => panic!("[ERROR] {err}"),
}
assert_eq!(
    matrix,
    CSVBinaryMatrix::try_from(&[
        [0, 0, 0, 1],
        [0, 0, 1, 1],
        [0, 1, 1, 1],
        [1, 1, 1, 1],
    ]).unwrap()
);
```
