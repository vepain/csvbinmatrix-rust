# CSV Binary Matrix Items

This module defines the items of [`CSVBinaryMatrix`].

[`CSVBinaryMatrix`]: crate::matrix::CSVBinaryMatrix