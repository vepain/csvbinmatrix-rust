# Compressed Sparse Vector (CSV) Binary Matrix Library.

[![Crates.io][crate_badge]][crate_link]
[![MSRV][msrv_badge]][crate_link]
[![Crate Downloads][downloads_badge]][crate_link]
[![Coverage report][coverage_badge]][coverage_link]
[![Pipeline status][pipeline_badge]][pipeline_link]
[![docs.rs][docs_badge]][docs_link]
[![License][license_badge]][licence_link]


🦀 Rust package for binary matrices represented in the Compressed Sparse Vector (CSV) format.

📖 **For more info on this library, check out the [docs.rs docs][docs_link].**

[`CSVBinaryMatrix`](https://docs.rs/csvbinmatrix/latest/csvbinmatrix/matrix/struct.CSVBinaryMatrix.html) is the main structure and maintains the CSV format which is suitable for sparse matrices.

This crate is based on the paper cited in the [References](#references) section.

* [Quick Usage](#quick-usage)
* [Tutorials](#tutorials)
* [Recent Changes](#recent-changes)
* [Contributing](#contributing)
* [License](#license)
* [References](#references)


## Quick Usage

```rust
# fn quick_usage() -> Result<(), Box<dyn std::error::Error>> {
use csvbinmatrix::prelude::CSVBinaryMatrix;

let matrix = match CSVBinaryMatrix::try_from(&[
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 1],
    [1, 1, 1],
]) {
    Ok(m) => m,
    Err(e) => panic!("[ERROR] Cannot create the matrix: {e}"),
};

println!("Matrix stats");
println!("------------");
println!(
    "Dimensions: {}x{} ({})",
    matrix.number_of_rows(),
    matrix.number_of_columns(),
    matrix.number_of_cells()
);
println!(
    "Number of ones/zeros: {}/{}",
    matrix.number_of_ones(),
    matrix.number_of_zeros()
);
println!("Density: {:.2}%", matrix.density() * 100.0);
println!("Sparsity: {:.2}%", matrix.sparsity() * 100.0);
println!();

println!("Coordinates of ones");
println!("-------------------");
println!("row\tcolumn");
println!("---\t-------");
for coordinates in matrix.iter_ones_coordinates() {
    println!("{}\t{}", coordinates.row(), coordinates.column());
}
println!();

match matrix.to_file("mymatrix.csvbm") {
    Ok(_) => println!("[INFO] File created"),
    Err(e) => println!("[ERROR] Creating the file fails: {e}"),
}

match CSVBinaryMatrix::try_from_file("mymatrix.csvbm") {
    Ok(m) => {
        println!("[INFO] Read from file");
        assert_eq!(m, matrix)
    }
    Err(e) => println!("[ERROR] Cannot read the file: {e}"),
}
#
#    std::fs::remove_file("mymatrix.csvbm").unwrap_or(());
#    Ok(())
# }
```


## Tutorials

* [Extend a matrix with rows or columns](https://docs.rs/csvbinmatrix/latest/csvbinmatrix/matrix/update/index.html)
* [Write a matrix into a file and initialize a new matrix from a file](https://docs.rs/csvbinmatrix/latest/csvbinmatrix/matrix/io/index.html)
* [Efficiently produce submatrices](https://docs.rs/csvbinmatrix/latest/csvbinmatrix/matrix/views/index.html)


## Recent Changes

See [`CHANGELOG.md`](https://gitlab.com/vepain/csvbinmatrix-rust/-/blob/main/CHANGELOG.md).


## Contributing

See [`CONTRIBUTING.md`](https://gitlab.com/vepain/csvbinmatrix-rust/-/blob/main/CONTRIBUTING.md).


## License

Dual-licensed to be compatible with the Rust project.

Licensed under the Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0> or the MIT license <http://opensource.org/licenses/MIT>, at your option. This file may not be copied, modified, or distributed except according to those terms.


## References

This package is based on this paper, where the authors' method is adapted for binary matrices:
> Farzaneh, Aiyoub, Hossein Kheırı, et Mehdi Abbaspour Shahmersı. « AN EFFICIENT STORAGE FORMAT FOR LARGE SPARSE MATRICES ». Communications Faculty of Sciences University of Ankara Series A1 Mathematics and Statistics 58, nᵒ 2 (1 août 2009): 1‑10. <https://doi.org/10.1501/Commua1_0000000648>.


<!-- Badges -->

[crate_badge]: https://img.shields.io/crates/v/csvbinmatrix?style=for-the-badge&logo=rust&color=blue "Crate badge"
[crate_link]: https://crates.io/crates/csvbinmatrix "Crate link"
[downloads_badge]: https://img.shields.io/crates/dv/csvbinmatrix.svg?style=for-the-badge&logo=rust "Crate downloads"
[msrv_badge]: https://img.shields.io/crates/msrv/csvbinmatrix?style=for-the-badge&logo=rust "Minimum Supported Rust Version"
[coverage_badge]: https://img.shields.io/gitlab/pipeline-coverage/vepain%2Fcsvbinmatrix-rust?job_name=test_coverage&branch=main&style=for-the-badge&logo=codecov "Coverage badge"
[coverage_link]: https://gitlab.com/vepain/csvbinmatrix-rust/-/commits/main "Coverage link"
[pipeline_badge]: https://img.shields.io/gitlab/pipeline-status/vepain%2Fcsvbinmatrix-rust?branch=main&style=for-the-badge&logo=circleci "Pipeline badge"
[pipeline_link]: https://gitlab.com/vepain/csvbinmatrix-rust/-/commits/main "Pipeline link"
[docs_badge]: https://img.shields.io/docsrs/csvbinmatrix?style=for-the-badge&logo=docsdotrs "docs.rs badge"
[docs_link]: https://docs.rs/csvbinmatrix/latest/csvbinmatrix/ "docs.rs link"
[license_badge]: https://img.shields.io/crates/l/csvbinmatrix.svg?style=for-the-badge&logo=readdotcv "Licence badge"
[licence_link]: https://gitlab.com/vepain/csvbinmatrix-rust "Licence link"