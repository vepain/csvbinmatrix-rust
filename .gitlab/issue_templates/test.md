## What to test

> One or several items from the same category to test.

## How to test

> If new test is needed.

## Test editing

> If a test should be edited.

/label ~test